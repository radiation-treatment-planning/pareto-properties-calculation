﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathNet.Spatial.Euclidean;
using OxyPlot;
using Pareto;
using ParetoFrontLineApproximation.Immutables;
using ParetoPropertiesCalculation.Strategies;

namespace ParetoPropertiesCalculation
{
    public class GlobalOptimiumCalculator
    {
        public IParetoBuildStrategy ParetoBuildStrategy { get; }

        public GlobalOptimiumCalculator(IParetoBuildStrategy paretoBuildStrategy)
        {
            ParetoBuildStrategy = paretoBuildStrategy;
        }

        /// <summary>
        /// Calculates the intersection point of diagonal line and pareto front line..
        /// </summary>
        /// <param name="paretoFrontLine">The approximated pareto front line.</param>
        /// <param name="diagonalLine">The diagonal line from illusion point to point of maximum coordinates of all points.</param>
        /// <param name="color">Color of point.</param>
        /// <param name="markerType">Marker type of point</param>
        /// <returns>Null, if no intersection could be calculated. ParetoPoint, otherwise.</returns>
        public ParetoPoint Calculate(ParetoFrontLine paretoFrontLine, ParetoLine diagonalLine, OxyColor color, MarkerType markerType)
        {
            var lineSegments = CreateLineSegmentsFromParetoFrontLinePoints(paretoFrontLine);
            return CalculateGlobalOptimum(lineSegments, diagonalLine, color, markerType);
        }

        private ParetoPoint CalculateGlobalOptimum(LineSegment2D[] lineSegments, ParetoLine diagonalLine, OxyColor color, MarkerType markerType)
        {
            Point2D globalOptimumPoint = default;
            foreach (var lineSegment in lineSegments)
            {
                var intersection = CalculateIntersectionPoint(lineSegment, diagonalLine);
                if (intersection != null) globalOptimumPoint = intersection.Value;
            }

            var tag = ParetoBuildStrategy.BuildTag(globalOptimumPoint);
            return ParetoPoint.Factory.CreateGlobalOptimumParetoPoint(tag, globalOptimumPoint, color, markerType);
        }

        private Point2D? CalculateIntersectionPoint(LineSegment2D lineSegment, ParetoLine diagonalLine)
        {
            var firstLineSlopeX = lineSegment.EndPoint.X - lineSegment.StartPoint.X;
            var firstLineSlopeY = lineSegment.EndPoint.Y - lineSegment.StartPoint.Y;

            var secondLineSlopeX = diagonalLine.Points.Last().X - diagonalLine.Points.First().X;
            var secondLineSlopeY = diagonalLine.Points.Last().Y - diagonalLine.Points.First().Y;

            var s = (-firstLineSlopeY * (lineSegment.StartPoint.X - diagonalLine.Points.First().X) +
                     firstLineSlopeX * (lineSegment.StartPoint.Y - diagonalLine.Points.First().Y)) /
                    (-secondLineSlopeX * firstLineSlopeY + firstLineSlopeX * secondLineSlopeY);
            var t = (secondLineSlopeX * (lineSegment.StartPoint.Y - diagonalLine.Points.First().Y) -
                     secondLineSlopeY * (lineSegment.StartPoint.X - diagonalLine.Points.First().X)) /
                    (-secondLineSlopeX * firstLineSlopeY + firstLineSlopeX * secondLineSlopeY);

            if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
            {
                var intersectionPointX = lineSegment.StartPoint.X + (t * firstLineSlopeX);
                var intersectionPointY = lineSegment.StartPoint.Y + (t * firstLineSlopeY);

                return new Point2D(intersectionPointX, intersectionPointY);
            }
            return null;
        }

        private LineSegment2D[] CreateLineSegmentsFromParetoFrontLinePoints(ParetoFrontLine paretoFrontLine)
        {
            if (paretoFrontLine.Vertices.Length < 2)
                throw new ArgumentException(
                    $"TcpNtcpPareto front line needs at least two vertices, but has {paretoFrontLine.Vertices.Length}");

            var lineSegments = new List<LineSegment2D>();
            var i = 0;
            var j = 1;
            var vertices = paretoFrontLine.Vertices;
            var numberOfVertices = paretoFrontLine.Vertices.Length;
            var tolerance = 0.0001;
            while (j < numberOfVertices - 1)
            {
                var leftPoint = vertices[i];
                var rightPoint = vertices[j];
                if (Math.Abs(leftPoint.X - rightPoint.X) > tolerance &&
                    Math.Abs(leftPoint.Y - rightPoint.Y) > tolerance)
                {
                    lineSegments.Add(new LineSegment2D(leftPoint, rightPoint));
                    i++;
                    j++;
                }
                else
                    j++;
            }

            return lineSegments.ToArray();
        }
    }
}
