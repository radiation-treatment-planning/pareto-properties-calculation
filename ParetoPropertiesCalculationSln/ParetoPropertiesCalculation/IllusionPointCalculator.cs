﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathNet.Spatial.Euclidean;
using OxyPlot;
using Pareto;
using ParetoPropertiesCalculation.Strategies;

namespace ParetoPropertiesCalculation
{
    public class IllusionPointCalculator
    {
        public IParetoBuildStrategy ParetoBuildStrategy { get; }

        public IllusionPointCalculator(IParetoBuildStrategy paretoBuildStrategy)
        {
            ParetoBuildStrategy = paretoBuildStrategy;
        }

        public ParetoPoint Calculate(IEnumerable<ParetoPoint> points, OxyColor color, MarkerType markerType)
        {
            if (!points.Any()) throw new ArgumentException("List of points must not be empty.");
            var smallestX = points.Min(x => x.Point.X);
            var smallestY = points.Min(x => x.Point.Y);
            var point = new Point2D(smallestX, smallestY);
            var tag = ParetoBuildStrategy.BuildTag(point);
            return ParetoPoint.Factory.CreateIllusionParetoPoint(tag, point,
                color, markerType);
        }
    }
}
