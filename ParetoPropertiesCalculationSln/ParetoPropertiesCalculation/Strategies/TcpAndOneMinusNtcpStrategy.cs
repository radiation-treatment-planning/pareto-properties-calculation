﻿using System;
using System.Collections.Generic;
using System.Globalization;
using MathNet.Spatial.Euclidean;
using Pareto;
using TcpNtcpCalculation;
using TcpNtcpCalculation.Helpers;
using TcpNtcpCalculation.Immutables;

namespace ParetoPropertiesCalculation.Strategies
{
    public class TcpAndOneMinusNtcpStrategy : IParetoBuildStrategy
    {
        public ComplicationFreeTumorControlCalculator ComplicationFreeTumorControlCalculator { get; }
        public uint NumberOfFloatingPointDigits { get; }
        public NumberFormatInfo DoubleToStringNumberFormatInfo { get; set; }

        public TcpAndOneMinusNtcpStrategy(
        ComplicationFreeTumorControlCalculator complicationFreeTumorControlCalculator,
        uint numberOfFloatingPointDigits)
        {
            ComplicationFreeTumorControlCalculator = complicationFreeTumorControlCalculator;
            NumberOfFloatingPointDigits = numberOfFloatingPointDigits;
            DoubleToStringNumberFormatInfo = new NumberFormatInfo() { NumberDecimalSeparator = "." };
        }
        public Point2D TransformPoint(TcpNtcpCftcPoint point)
        {
            return new Point2D(point.TumorControlProbability.Value,
                1.0 - point.NormalTissueComplicationProbability.Value);
        }

        public object BuildTag(TcpNtcpCftcPoint point)
        {
            return ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
            {
                ("Patient Piz", point.Tag.PatientPiz),
                ("Course Id", point.Tag.CourseId),
                ("Plan Id", point.Tag.PlanId),
                ("TCP", Round(point.TumorControlProbability.Value)),
                ("NTCP", Round(point.NormalTissueComplicationProbability.Value)),
                ("1-NTCP", Round(1-point.NormalTissueComplicationProbability.Value)),
                ("P+", Round(point.ComplicationFreeTumorControl.Value))
            });
        }

        public object BuildTag(Point2D point)
        {
            var tcpNtcpResult = new TcpNtcpResult(new TumorControlProbability(point.X),
                new NormalTissueComplicationProbability(1.0 - point.Y));
            var cftc = ComplicationFreeTumorControlCalculator.Calculate(tcpNtcpResult);
            return ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>(){
                ("TCP", Round(tcpNtcpResult.TumorControlProbability.Value)),
                ("NTCP", Round(tcpNtcpResult.NormalTissueComplicationProbability.Value)),
                ("1-NTCP", Round(1.0 - tcpNtcpResult.NormalTissueComplicationProbability.Value)),
                ("P+", Round(cftc.Value))

            });
        }

        public string Description { get; } = "Calculate pareto with TCP and 1-NTCP";
        private string Round(double number) => Math.Round(number, (int)NumberOfFloatingPointDigits).ToString(DoubleToStringNumberFormatInfo);
    }
}
