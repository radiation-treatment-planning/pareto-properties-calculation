﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pareto;
using TcpNtcpCalculation.Helpers;

namespace ParetoPropertiesCalculation.Strategies
{
    internal interface IParetoTagBuilderStrategy
    {
        ParetoTag BuildParetoTag(TcpNtcpCftcPoint point);
    }
}
