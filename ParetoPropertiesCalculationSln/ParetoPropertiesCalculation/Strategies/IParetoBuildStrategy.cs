﻿using MathNet.Spatial.Euclidean;
using TcpNtcpCalculation.Helpers;

namespace ParetoPropertiesCalculation.Strategies
{
    public interface IParetoBuildStrategy
    {
        Point2D TransformPoint(TcpNtcpCftcPoint point);
        object BuildTag(TcpNtcpCftcPoint point);
        object BuildTag(Point2D point);
        string Description { get; }
    }
}
