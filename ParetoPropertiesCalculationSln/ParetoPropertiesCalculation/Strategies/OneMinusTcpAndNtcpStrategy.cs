﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Pareto;
using TcpNtcpCalculation.Helpers;
using MathNet.Spatial.Euclidean;
using TcpNtcpCalculation;
using TcpNtcpCalculation.Immutables;

namespace ParetoPropertiesCalculation.Strategies
{
    public class OneMinusTcpAndNtcpStrategy : IParetoBuildStrategy
    {
        public ComplicationFreeTumorControlCalculator ComplicationFreeTumorControlCalculator { get; }
        public uint NumberOfFloatingPointDigits { get; }
        public NumberFormatInfo DoubleToStringNumberFormatInfo { get; set; }

        public OneMinusTcpAndNtcpStrategy(
            ComplicationFreeTumorControlCalculator complicationFreeTumorControlCalculator,
            uint numberOfFloatingPointDigits)
        {
            ComplicationFreeTumorControlCalculator = complicationFreeTumorControlCalculator;
            NumberOfFloatingPointDigits = numberOfFloatingPointDigits;
            DoubleToStringNumberFormatInfo = new NumberFormatInfo() { NumberDecimalSeparator = "." };
        }

        public Point2D TransformPoint(TcpNtcpCftcPoint point)
        {
            return new Point2D(1.0 - point.TumorControlProbability.Value,
                point.NormalTissueComplicationProbability.Value);
        }

        public object BuildTag(TcpNtcpCftcPoint point)
        {
            return ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
            {
                ("Patient Piz", point.Tag.PatientPiz),
                ("Course Id", point.Tag.CourseId),
                ("Plan Id", point.Tag.PlanId),
                ("TCP", Round(point.TumorControlProbability.Value)),
                ("1-TCP", Round((1 - point.TumorControlProbability.Value))),
                ("NTCP", Round(point.NormalTissueComplicationProbability.Value)),
                ("P+", Round(point.ComplicationFreeTumorControl.Value))
            });
        }

        public object BuildTag(Point2D point)
        {
            var tcpNtcpResult = new TcpNtcpResult(new TumorControlProbability(1.0 - point.X),
                new NormalTissueComplicationProbability(point.Y));
            var cftc = ComplicationFreeTumorControlCalculator.Calculate(tcpNtcpResult);
            return ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>(){
                    ("TCP", Round(tcpNtcpResult.TumorControlProbability.Value)),
                    ("1-TCP", Round(1.0 - tcpNtcpResult.TumorControlProbability.Value)),
                    ("NTCP", Round(tcpNtcpResult.NormalTissueComplicationProbability.Value)),
                    ("P+", Round(cftc.Value))

            });
        }

        public string Description { get; } = "Calculate pareto with 1-TCP and NTCP";
        private string Round(double number) => Math.Round(number, (int)NumberOfFloatingPointDigits).ToString(DoubleToStringNumberFormatInfo);
    }
}
