﻿using System.Collections.Generic;
using System.Linq;
using Pareto;
using ParetoFrontLineApproximation;
using ParetoPropertiesCalculation.Converters;
using ParetoPropertiesCalculation.Immutables;
using TcpNtcpCalculation.Helpers;

namespace ParetoPropertiesCalculation
{
    public class ParetoPropertiesCalculator
    {
        public ParetoFrontLineConverter ParetoFrontLineConverter { get; }
        public TcpNtcpCftcPointsToParetoPointsConverter TcpNtcpCftcPointsToParetoPointsConverter { get; }
        public ParetoDominatingPointsCalculator ParetoDominatingPointsCalculator { get; }
        public IllusionPointCalculator IllusionPointCalculator { get; }
        public ParetoFrontLineApproximator ParetoFrontLineApproximator { get; }
        public DiagonalLineCalculator DiagonalLineCalculator { get; }
        public GlobalOptimiumCalculator GlobalOptimiumCalculator { get; }
        public ParetoTheme ParetoTheme { get; }

        public ParetoPropertiesCalculator(
            ParetoFrontLineConverter paretoFrontLineConverter,
            TcpNtcpCftcPointsToParetoPointsConverter tcpNtcpCftcPointsToParetoPointsConverter,
            ParetoDominatingPointsCalculator paretoDominatingPointsCalculator,
            IllusionPointCalculator illusionPointCalculator,
            ParetoFrontLineApproximator paretoFrontLineApproximator,
            DiagonalLineCalculator diagonalLineCalculator,
            GlobalOptimiumCalculator globalOptimiumCalculator,
            ParetoTheme paretoTheme)
        {
            ParetoFrontLineConverter = paretoFrontLineConverter;
            TcpNtcpCftcPointsToParetoPointsConverter = tcpNtcpCftcPointsToParetoPointsConverter;
            ParetoDominatingPointsCalculator = paretoDominatingPointsCalculator;
            IllusionPointCalculator = illusionPointCalculator;
            ParetoFrontLineApproximator = paretoFrontLineApproximator;
            DiagonalLineCalculator = diagonalLineCalculator;
            GlobalOptimiumCalculator = globalOptimiumCalculator;
            ParetoTheme = paretoTheme;
        }
        public Pareto.Pareto CalculateAllParetoProperties(IEnumerable<TcpNtcpCftcPoint> points)
        {
            var convertedPoints = TcpNtcpCftcPointsToParetoPointsConverter.Convert(points);
            return CalculateAllParetoProperties(convertedPoints);
        }

        public Pareto.Pareto CalculateAllParetoProperties(IEnumerable<ParetoPoint> points)
        {
            var illusionPoint = IllusionPointCalculator.Calculate(points, ParetoTheme.IllusionParetoPointColor,
                ParetoTheme.IllusionParetoPointMarkerType);
            var (paretoDominatingPoints, paretoDominatedPoints) = ParetoDominatingPointsCalculator.Calculate(points, ParetoTheme.DominatingParetoPointColor,
                ParetoTheme.DominatingParetoPointMarkerType);
            var paretoDominatingPoint2Ds = paretoDominatingPoints.Select(x => x.Point);
            var paretoFrontLine = ParetoFrontLineApproximator.Approximate(paretoDominatingPoint2Ds);
            var convertedParetoFrontLine = ParetoFrontLineConverter.Convert(paretoFrontLine, ParetoTheme.ParetoFrontLineColor, ParetoTheme.ParetoFrontLineLineStyle);
            var diagonalLine = DiagonalLineCalculator.Calculate(points, ParetoTheme.DiagonalLineColor, ParetoTheme.DiagonalLineLineStyle);
            var globalOptimum = GlobalOptimiumCalculator.Calculate(paretoFrontLine, diagonalLine, ParetoTheme.GlobalOptimumParetoPointColor, ParetoTheme.GlobalOptimumParetoPointMarkerType);
            var allParetoPoints = CombineAllParetoPoints(illusionPoint, paretoDominatingPoints, paretoDominatedPoints, globalOptimum);
            var allParetoLines = new[] { diagonalLine, convertedParetoFrontLine };

            return new Pareto.Pareto(allParetoPoints.ToArray(), allParetoLines);
        }

        private List<ParetoPoint> CombineAllParetoPoints(ParetoPoint illusionPoint,
            IEnumerable<ParetoPoint> paretoDominatingPoints,
            IEnumerable<ParetoPoint> paretoDominatedPoints,
            ParetoPoint globalOptimum)
        {
            var allPoints = new List<ParetoPoint>(paretoDominatedPoints);
            allPoints.AddRange(paretoDominatingPoints);
            allPoints.Add(illusionPoint);
            allPoints.Add(globalOptimum);
            return allPoints;
        }
    }
}