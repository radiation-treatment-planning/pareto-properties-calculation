﻿using MathNet.Spatial.Euclidean;
using System;
using System.Collections.Generic;
using System.Linq;
using OxyPlot;
using Pareto;

namespace ParetoPropertiesCalculation
{
    public class DiagonalLineCalculator
    {
        public ParetoLine Calculate(IEnumerable<ParetoPoint> points, OxyColor color, LineStyle lineStyle)
        {
            if (points.Count() < 2)
                throw new ArgumentException("At least two points are necessary to calculate diagonal line.");
            var maxX = points.Max(x => x.Point.X);
            var maxY = points.Max(x => x.Point.Y);
            var highestDistance = maxX > maxY ? maxX : maxY;
            var endOfDiagonalLineX = highestDistance;
            var endOfDiagonalLineY = highestDistance;

            return ParetoLine.Factory.CreateDiagonalLine("",
                new Point2D[] { Point2D.Origin, new Point2D(endOfDiagonalLineX, endOfDiagonalLineY) },
                color, lineStyle);
        }
    }
}
