﻿using System.Collections.Generic;
using System.Linq;
using OxyPlot;
using Pareto;

namespace ParetoPropertiesCalculation
{
    public class ParetoDominatingPointsCalculator
    {
        private readonly ParetoDominationCalculator _paretoDominationCalculator;

        public ParetoDominatingPointsCalculator(ParetoDominationCalculator paretoDominationCalculator)
        {
            _paretoDominationCalculator = paretoDominationCalculator;
        }

        public (IEnumerable<ParetoPoint> paretoDominatingPoints, IEnumerable<ParetoPoint> paretoDominatedPoints)
            Calculate(IEnumerable<ParetoPoint> allPoints,
                OxyColor paretoDominatingPointColor,
                MarkerType paretoDominatingPointMarkerType)
        {
            var points = allPoints.ToArray();
            var paretoDominatingPoints = new HashSet<ParetoPoint>();
            var paretoDominatedPoints = new HashSet<ParetoPoint>();


            for (var i = 0; i < points.Length; i++)
            {
                var pointToCheck = points[i];
                var isDominating = true;

                for (var j = 0; j < points.Length; j++)
                {
                    var nextPoint = points[j];
                    if (_paretoDominationCalculator.IsDominating(pointToCheck, nextPoint)) continue;
                    if (!_paretoDominationCalculator.IsDominating(nextPoint, pointToCheck)) continue;
                    isDominating = false;
                    break;
                }

                if (isDominating)
                    paretoDominatingPoints.Add(ParetoPoint.Factory.CreateDominatingParetoPoint(pointToCheck.Tag,
                        pointToCheck.Point, paretoDominatingPointColor, paretoDominatingPointMarkerType));
                else
                    paretoDominatedPoints.Add(ParetoPoint.Factory.CreateParetoPoint(pointToCheck.Tag, pointToCheck.Point,
                        pointToCheck.Color, pointToCheck.MarkerType));
            }

            return (paretoDominatingPoints, paretoDominatedPoints);
        }
    }
}
