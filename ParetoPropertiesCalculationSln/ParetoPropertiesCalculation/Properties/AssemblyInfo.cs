﻿using System.Reflection;
using System.Runtime.InteropServices;

// Allgemeine Informationen über eine Assembly werden über die folgenden
// Attribute gesteuert. Ändern Sie diese Attributwerte, um die Informationen zu ändern,
// die einer Assembly zugeordnet sind.
[assembly: AssemblyTitle("ParetoPropertiesCalculation")]
[assembly: AssemblyDescription("A .Net Framework 4.7.2 library for calculating diverse Pareto properties: Pareto optimal points, illusion point, diagonal line through pareto front line, global optimum.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Medical Center University of Freiburg")]
[assembly: AssemblyProduct("ParetoPropertiesCalculation")]
[assembly: AssemblyCopyright("Copyright © Dejan Kostyszyn 2022")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Durch Festlegen von ComVisible auf FALSE werden die Typen in dieser Assembly
// für COM-Komponenten unsichtbar.  Wenn Sie auf einen Typ in dieser Assembly von
// COM aus zugreifen müssen, sollten Sie das ComVisible-Attribut für diesen Typ auf "True" festlegen.
[assembly: ComVisible(false)]

// Die folgende GUID bestimmt die ID der Typbibliothek, wenn dieses Projekt für COM verfügbar gemacht wird
[assembly: Guid("cd9bfd23-83b7-4915-8dcc-932596b7e90d")]

// Versionsinformationen für eine Assembly bestehen aus den folgenden vier Werten:
//
//      Hauptversion
//      Nebenversion
//      Buildnummer
//      Revision
//
// Sie können alle Werte angeben oder Standardwerte für die Build- und Revisionsnummern verwenden,
// indem Sie "*" wie unten gezeigt eingeben:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("6.5.1")]
[assembly: AssemblyFileVersion("6.5.1")]
