﻿using OxyPlot;

namespace ParetoPropertiesCalculation.Immutables
{
    public class ParetoTheme
    {
        public OxyColor IllusionParetoPointColor { get; }
        public OxyColor GlobalOptimumParetoPointColor { get; }
        public OxyColor DominatingParetoPointColor { get; }
        public OxyColor DiagonalLineColor { get; }
        public OxyColor ParetoFrontLineColor { get; }
        public OxyColor ParetoLineColor { get; }

        public MarkerType IllusionParetoPointMarkerType { get; }
        public MarkerType GlobalOptimumParetoPointMarkerType { get; }
        public MarkerType DominatingParetoPointMarkerType { get; }
        public LineStyle DiagonalLineLineStyle { get; }
        public LineStyle ParetoFrontLineLineStyle { get; }
        public LineStyle ParetoLineLineStyle { get; }

        /// <summary>
        /// Defines the theme for pareto points and lines, including color, marker type and line style.
        /// </summary>
        /// <param name="illusionParetoPointColor">Color of Illusion Pareto points.</param>
        /// <param name="globalOptimumParetoPointColor">Color of global optimum Pareto points.</param>
        /// <param name="dominatingParetoPointColor">Color of dominating Pareto points.</param>
        /// <param name="diagonalLineColor">Color of diagonal lines.</param>
        /// <param name="paretoFrontLineColor">Color of Pareto front lines</param>
        /// <param name="paretoLineColor">Color of Pareto lines.</param>
        /// <param name="illusionParetoPointMarkerType">Marker type of Illusion Pareto points.</param>
        /// <param name="globalOptimumParetoPointMarkerType">Marker type of global optimum Pareto points.</param>
        /// <param name="dominatingParetoPointMarkerType">Marker type of dominating Pareto points.</param>
        /// <param name="diagonalLineLineStyle">Line style of diagonal lines.</param>
        /// <param name="paretoFrontLineLineStyle">Line style Pareto front lines.</param>
        /// <param name="paretoLineLineStyle">Line style of Pareto lines.</param>
        private ParetoTheme(
            OxyColor illusionParetoPointColor,
            OxyColor globalOptimumParetoPointColor,
            OxyColor dominatingParetoPointColor,
            OxyColor diagonalLineColor,
            OxyColor paretoFrontLineColor,
            OxyColor paretoLineColor,
            MarkerType illusionParetoPointMarkerType,
            MarkerType globalOptimumParetoPointMarkerType,
            MarkerType dominatingParetoPointMarkerType,
            LineStyle diagonalLineLineStyle,
            LineStyle paretoFrontLineLineStyle,
            LineStyle paretoLineLineStyle)
        {
            IllusionParetoPointColor = illusionParetoPointColor;
            GlobalOptimumParetoPointColor = globalOptimumParetoPointColor;
            DominatingParetoPointColor = dominatingParetoPointColor;
            DiagonalLineColor = diagonalLineColor;
            ParetoFrontLineColor = paretoFrontLineColor;
            ParetoLineColor = paretoLineColor;
            IllusionParetoPointMarkerType = illusionParetoPointMarkerType;
            GlobalOptimumParetoPointMarkerType = globalOptimumParetoPointMarkerType;
            DominatingParetoPointMarkerType = dominatingParetoPointMarkerType;
            DiagonalLineLineStyle = diagonalLineLineStyle;
            ParetoFrontLineLineStyle = paretoFrontLineLineStyle;
            ParetoLineLineStyle = paretoLineLineStyle;
        }

        /// <summary>
        /// Defines a custom theme for pareto points and lines, including color, marker type and line style.
        /// </summary>
        /// <param name="illusionParetoPointColor">Color of Illusion Pareto points.</param>
        /// <param name="globalOptimumParetoPointColor">Color of global optimum Pareto points.</param>
        /// <param name="dominatingParetoPointColor">Color of dominating Pareto points.</param>
        /// <param name="diagonalLineColor">Color of diagonal lines.</param>
        /// <param name="paretoFrontLineColor">Color of Pareto front lines</param>
        /// <param name="paretoLineColor">Color of Pareto lines.</param>
        /// <param name="illusionParetoPointMarkerType">Marker type of Illusion Pareto points.</param>
        /// <param name="globalOptimumParetoPointMarkerType">Marker type of global optimum Pareto points.</param>
        /// <param name="dominatingParetoPointMarkerType">Marker type of dominating Pareto points.</param>
        /// <param name="diagonalLineLineStyle">Line style of diagonal lines.</param>
        /// <param name="paretoFrontLineLineStyle">Line style Pareto front lines.</param>
        /// <param name="paretoLineLineStyle">Line style of Pareto lines.</param>
        /// <returns>A custom ParetoTheme.</returns>
        public static ParetoTheme CreateCustom(
            OxyColor illusionParetoPointColor,
            OxyColor globalOptimumParetoPointColor,
            OxyColor dominatingParetoPointColor,
            OxyColor diagonalLineColor,
            OxyColor paretoFrontLineColor,
            OxyColor paretoLineColor,
            MarkerType illusionParetoPointMarkerType,
            MarkerType globalOptimumParetoPointMarkerType,
            MarkerType dominatingParetoPointMarkerType,
            LineStyle diagonalLineLineStyle,
            LineStyle paretoFrontLineLineStyle,
            LineStyle paretoLineLineStyle)
        {
            return new ParetoTheme(illusionParetoPointColor, globalOptimumParetoPointColor,
                dominatingParetoPointColor, diagonalLineColor, paretoFrontLineColor, paretoLineColor,
                illusionParetoPointMarkerType, globalOptimumParetoPointMarkerType,
                dominatingParetoPointMarkerType, diagonalLineLineStyle, paretoFrontLineLineStyle, paretoLineLineStyle);
        }

        /// <summary>
        /// Defines a default theme for pareto points and lines, including color, marker type and line style.
        /// </summary>
        /// <returns>A default ParetoTheme.</returns>
        public static ParetoTheme CreateDefault()
        {
            return new ParetoTheme(OxyColors.Goldenrod, OxyColors.DarkGreen,
                OxyColors.DarkRed, OxyColors.Goldenrod, OxyColors.DarkRed, OxyColors.DarkOrange,
                MarkerType.Triangle, MarkerType.Diamond, MarkerType.Square, LineStyle.Dash, LineStyle.Solid,
                LineStyle.Dot);
        }
    }
}
