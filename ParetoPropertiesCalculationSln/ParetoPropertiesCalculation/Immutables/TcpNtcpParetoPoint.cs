﻿using ParetoPropertiesCalculation.Strategies;
using RadiationTreatmentPlanner.Utils.Point;
using TcpNtcpCalculation.Helpers.Immutables;
using TcpNtcpCalculation.Immutables;

namespace ParetoPropertiesCalculation.Immutables
{
    public class TcpNtcpParetoPoint
    {
        public TumorControlProbability TumorControlProbability { get; }
        public NormalTissueComplicationProbability NormalTissueComplicationProbability { get; }
        public ComplicationFreeTumorControl ComplicationFreeTumorControl { get; }
        public UPoint TransformedPoint { get; }
        public UTag Tag { get; }
        public IParetoBuildStrategy ParetoBuildStrategy { get; }

        public TcpNtcpParetoPoint(TumorControlProbability tumorControlProbability, NormalTissueComplicationProbability normalTissueComplicationProbability, ComplicationFreeTumorControl complicationFreeTumorControl, UPoint transformedPoint, UTag tag, IParetoBuildStrategy paretoBuildStrategy)
        {
            TumorControlProbability = tumorControlProbability;
            NormalTissueComplicationProbability = normalTissueComplicationProbability;
            ComplicationFreeTumorControl = complicationFreeTumorControl;
            TransformedPoint = transformedPoint;
            Tag = tag;
            ParetoBuildStrategy = paretoBuildStrategy;
        }
    }
}
