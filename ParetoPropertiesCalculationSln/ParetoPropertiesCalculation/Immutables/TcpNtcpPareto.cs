﻿using MathNet.Spatial.Euclidean;
using Optional;
using ParetoFrontLineApproximation.Immutables;
using ParetoPropertiesCalculation.Immutables;
using RadiationTreatmentPlanner.Utils.Point;

namespace ParetoPropertiesCalculation
{
    public class TcpNtcpPareto
    {
        public TcpNtcpParetoPoint[] AllPoints { get; }
        public TcpNtcpParetoPoint[] ParetoOptimalPoints { get; }
        public TcpNtcpParetoPoint PointWithMaximumComplicationFreeTumorControl { get; }
        public UPoint IllusionPoint { get; }
        public ParetoFrontLine ParetoFrontLine { get; }
        public LineSegment2D DiagonalLine { get; }
        public Option<Point2D> GlobalOptimum { get; }

        public TcpNtcpPareto(TcpNtcpParetoPoint[] allPoints,
            TcpNtcpParetoPoint[] paretoOptimalPoints,
            TcpNtcpParetoPoint pointWithMaximumComplicationFreeTumorControl,
            UPoint illusionPoint,
            ParetoFrontLine paretoFrontLine,
            LineSegment2D diagonalLine,
            Option<Point2D> globalOptimum)
        {
            AllPoints = allPoints;
            ParetoOptimalPoints = paretoOptimalPoints;
            PointWithMaximumComplicationFreeTumorControl = pointWithMaximumComplicationFreeTumorControl;
            IllusionPoint = illusionPoint;
            ParetoFrontLine = paretoFrontLine;
            DiagonalLine = diagonalLine;
            GlobalOptimum = globalOptimum;
        }
    }
}
