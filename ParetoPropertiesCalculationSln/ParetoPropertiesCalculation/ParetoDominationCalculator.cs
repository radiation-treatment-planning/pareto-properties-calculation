﻿using Pareto;

namespace ParetoPropertiesCalculation
{
    public class ParetoDominationCalculator
    {
        /// <summary>
        /// Calculates whether one point is pareto domination a second point.
        /// </summary>
        /// <param name="point1">First pareto point</param>
        /// <param name="point2">Second pareto point</param>
        /// <returns>True, if point 1 is pareto dominating point2. False, otherwise.</returns>
        public bool IsDominating(ParetoPoint point1, ParetoPoint point2)
        {
            var a = point1.Point;
            var b = point2.Point;
            return (a.X <= b.X && a.Y <= b.Y) && (a.X < b.X || a.Y < b.Y);
        }
    }
}
