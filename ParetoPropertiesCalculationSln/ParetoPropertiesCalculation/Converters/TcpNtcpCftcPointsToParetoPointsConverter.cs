﻿using System.Collections.Generic;
using OxyPlot;
using Pareto;
using ParetoPropertiesCalculation.Strategies;
using TcpNtcpCalculation.Helpers;

namespace ParetoPropertiesCalculation
{
    public class TcpNtcpCftcPointsToParetoPointsConverter
    {
        private readonly IParetoBuildStrategy _paretoBuildStrategy;

        public TcpNtcpCftcPointsToParetoPointsConverter(IParetoBuildStrategy paretoBuildStrategy)
        {
            _paretoBuildStrategy = paretoBuildStrategy;
        }

        public IEnumerable<ParetoPoint> Convert(IEnumerable<TcpNtcpCftcPoint> points)
        {
            var paretoPoints = new List<ParetoPoint>();
            foreach (var point in points)
            {
                var tag = _paretoBuildStrategy.BuildTag(point);
                var transformedPoint = _paretoBuildStrategy.TransformPoint(point);
                var paretoPoint = ParetoPoint.Factory.CreateParetoPoint(tag, transformedPoint,
                    OxyColors.Black, MarkerType.Circle);
                paretoPoints.Add(paretoPoint);
            }

            return paretoPoints;
        }

    }
}
