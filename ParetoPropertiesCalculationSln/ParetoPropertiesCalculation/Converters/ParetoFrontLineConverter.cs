﻿using OxyPlot;
using Pareto;
using ParetoFrontLineApproximation.Immutables;

namespace ParetoPropertiesCalculation.Converters
{
    public class ParetoFrontLineConverter
    {
        public ParetoLine Convert(ParetoFrontLine paretoFrontLine, OxyColor color, LineStyle lilneStyle)
        {
            return ParetoLine.Factory.CreateParetoFrontLine("", paretoFrontLine.Vertices, color,
                lilneStyle);
        }
    }
}