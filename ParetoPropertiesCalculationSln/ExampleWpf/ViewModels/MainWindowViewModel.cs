﻿using System;
using System.Collections.Generic;
using MathNet.Spatial.Euclidean;
using OxyPlot;
using Pareto;
using ParetoFrontLineApproximation;
using ParetoPropertiesCalculation;
using ParetoPropertiesCalculation.Converters;
using ParetoPropertiesCalculation.Immutables;
using ParetoPropertiesCalculation.Strategies;
using Prism.Mvvm;
using TcpNtcpCalculation;

namespace ExampleWpf.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private string _title = "Prism Application";
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private Pareto.Pareto _customParetoData;
        private readonly ParetoPropertiesCalculator _paretoPropertiesCalculator;

        public Pareto.Pareto CustomParetoData
        {
            get { return _customParetoData; }
            set { SetProperty(ref _customParetoData, value); }
        }

        private PlotController _customPlotController;
        public PlotController CustomPlotController
        {
            get => _customPlotController;
            set => SetProperty(ref _customPlotController, value);
        }

        public MainWindowViewModel()
        {
            CustomPlotController = new PlotController();
            CustomPlotController.UnbindMouseDown(OxyMouseButton.Left);
            CustomPlotController.BindMouseEnter(PlotCommands.HoverSnapTrack);

            _paretoPropertiesCalculator = InitializeParetoPropertiesCalculator();
            var randomParetoPoints = CreateARandomPareto();
            var paretoWithParetoProperties =
                _paretoPropertiesCalculator.CalculateAllParetoProperties(randomParetoPoints);
            CustomParetoData = paretoWithParetoProperties;
        }

        private ParetoPoint[] CreateARandomPareto()
        {
            var numberOfPoints = 100;
            var minValue = 0.1;
            var maxValue = 1.0;
            var randomGenerator = new Random();
            var paretoPoints = new ParetoPoint[numberOfPoints];
            for (int i = 0; i < numberOfPoints - 1; i++)
            {
                var angle = randomGenerator.NextDouble() * (Math.PI / 4 - minValue) + minValue;
                var value = randomGenerator.NextDouble() * (maxValue - minValue) + minValue;

                var x = Math.Sqrt(value) * Math.Cos(angle) / 2;
                var y = Math.Sqrt(value) * Math.Sin(angle) * 8 / 9;

                var tag = ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)> { ("Id", $"{i}") });
                var paretoPoint =
                    ParetoPoint.Factory.CreateParetoPoint(tag, new Point2D(x, y), OxyColors.Black, MarkerType.Circle);
                paretoPoints[i] = paretoPoint;
            }

            paretoPoints[numberOfPoints - 1] = ParetoPoint.Factory.CreateUniqueParetoPoint(
                ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)> { ("Id", $"{numberOfPoints}") }),
                new Point2D(0.5, 0.5), OxyColors.BlueViolet, MarkerType.Triangle, "Very important point");

            return paretoPoints;
        }

        private ParetoPropertiesCalculator InitializeParetoPropertiesCalculator()
        {
            var novelVertexCalculator = new NovelVertexCalculator();
            var normalVectorCalculator = new NormalVectorCalculator();
            var vertexWeightsRepository = new VertexWeightsRepository();
            var paretoFrontLineApproximator = new ParetoFrontLineApproximator(
                new AnchorPointsAndInteriorPointCalculator(novelVertexCalculator),
                new ConvexHullCalculator(new LineSegmentsFromPointsCreator()),
                new UpperParetoSurfaceCalculator(normalVectorCalculator),
                new VertexWeightsManager(normalVectorCalculator),
                new LowerDistalPointsCalculator(normalVectorCalculator, new HyperplaneBuilder(), new VertexValidator()),
                new FacetSelector(vertexWeightsRepository), new NextPointCalculator(normalVectorCalculator, new NovelVertexCalculator(), vertexWeightsRepository),
                new BoundingBoxCalculator(new HyperplaneBuilder(), vertexWeightsRepository), new VertexValidator(),
                new AdditionalSolutionsCalculator(new LineSegmentsFromPointsCreator()),
                vertexWeightsRepository);
            var paretoTheme = ParetoTheme.CreateDefault();
            var paretoBuildStrategy = new OneMinusTcpAndNtcpStrategy(new ComplicationFreeTumorControlCalculator(), 2);
            return new ParetoPropertiesCalculator(new ParetoFrontLineConverter(),
                new TcpNtcpCftcPointsToParetoPointsConverter(paretoBuildStrategy),
                new ParetoDominatingPointsCalculator(new ParetoDominationCalculator()), new IllusionPointCalculator(paretoBuildStrategy),
                paretoFrontLineApproximator, new DiagonalLineCalculator(), new GlobalOptimiumCalculator(paretoBuildStrategy),
                paretoTheme);
        }
    }
}
