﻿using NUnit.Framework;
using OxyPlot;
using ParetoPropertiesCalculation.Immutables;

namespace ParetoPropertiesCalculation.Tests
{
    [TestFixture]
    public class ParetoThemeTests
    {
        [Test]
        public void CreateDefault_Test()
        {
            var result = ParetoTheme.CreateDefault();
            Assert.AreEqual(OxyColors.Goldenrod, result.IllusionParetoPointColor);
            Assert.AreEqual(OxyColors.DarkGreen, result.GlobalOptimumParetoPointColor);
            Assert.AreEqual(OxyColors.DarkRed, result.DominatingParetoPointColor);

            Assert.AreEqual(OxyColors.Goldenrod, result.DiagonalLineColor);
            Assert.AreEqual(OxyColors.DarkRed, result.ParetoFrontLineColor);
            Assert.AreEqual(OxyColors.DarkOrange, result.ParetoLineColor);

            Assert.AreEqual(MarkerType.Triangle, result.IllusionParetoPointMarkerType);
            Assert.AreEqual(MarkerType.Diamond, result.GlobalOptimumParetoPointMarkerType);
            Assert.AreEqual(MarkerType.Square, result.DominatingParetoPointMarkerType);

            Assert.AreEqual(LineStyle.Dash, result.DiagonalLineLineStyle);
            Assert.AreEqual(LineStyle.Solid, result.ParetoFrontLineLineStyle);
            Assert.AreEqual(LineStyle.Dot, result.ParetoLineLineStyle);
        }
        [Test]
        public void CreateCustom_Test()
        {
            var result = ParetoTheme.CreateCustom(OxyColors.Gray, OxyColors.Yellow,
                OxyColors.Purple, OxyColors.Silver, OxyColors.Gold, OxyColors.Orange,
                MarkerType.Circle, MarkerType.Square, MarkerType.Diamond,
                LineStyle.Solid, LineStyle.Dot, LineStyle.Dash);

            Assert.AreEqual(OxyColors.Gray, result.IllusionParetoPointColor);
            Assert.AreEqual(OxyColors.Yellow, result.GlobalOptimumParetoPointColor);
            Assert.AreEqual(OxyColors.Purple, result.DominatingParetoPointColor);

            Assert.AreEqual(OxyColors.Silver, result.DiagonalLineColor);
            Assert.AreEqual(OxyColors.Gold, result.ParetoFrontLineColor);
            Assert.AreEqual(OxyColors.Orange, result.ParetoLineColor);

            Assert.AreEqual(MarkerType.Circle, result.IllusionParetoPointMarkerType);
            Assert.AreEqual(MarkerType.Square, result.GlobalOptimumParetoPointMarkerType);
            Assert.AreEqual(MarkerType.Diamond, result.DominatingParetoPointMarkerType);

            Assert.AreEqual(LineStyle.Solid, result.DiagonalLineLineStyle);
            Assert.AreEqual(LineStyle.Dot, result.ParetoFrontLineLineStyle);
            Assert.AreEqual(LineStyle.Dash, result.ParetoLineLineStyle);
        }
    }
}
