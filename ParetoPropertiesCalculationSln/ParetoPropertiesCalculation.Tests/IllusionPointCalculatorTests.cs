﻿using System;
using System.Collections.Generic;
using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using OxyPlot;
using Pareto;
using ParetoPropertiesCalculation.Strategies;
using TcpNtcpCalculation;

namespace ParetoPropertiesCalculation.Tests
{
    [TestFixture]
    public class IllusionPointCalculatorTests
    {
        [Test]
        public void Calculate_EmptyInput_Test()
        {
            var points = new List<ParetoPoint>();
            var illusionPointCalculator = new IllusionPointCalculator(new OneMinusTcpAndNtcpStrategy(new ComplicationFreeTumorControlCalculator(), 2));

            Assert.Throws<ArgumentException>(() => illusionPointCalculator.Calculate(points, OxyColors.Black, MarkerType.Circle));
        }

        [Test]
        public void Calculate_Test()
        {
            var points = new List<ParetoPoint>() {
                ParetoPoint.Factory.CreateParetoPoint("", new Point2D(0.1, 0.7), OxyColors.Black, MarkerType.Circle),
                ParetoPoint.Factory.CreateParetoPoint("", new Point2D(0.3, 0.3), OxyColors.Black, MarkerType.Circle),
                ParetoPoint.Factory.CreateParetoPoint("", new Point2D(0.4, 0.2), OxyColors.Black, MarkerType.Circle),
                ParetoPoint.Factory.CreateParetoPoint("", new Point2D(0.5, 0.5), OxyColors.Black, MarkerType.Circle),
            };

            var illusionPointCalculator = new IllusionPointCalculator(new OneMinusTcpAndNtcpStrategy(new ComplicationFreeTumorControlCalculator(), 2));
            var result = illusionPointCalculator.Calculate(points, OxyColors.Black, MarkerType.Circle);
            Assert.AreEqual(0.1, result.Point.X);
            Assert.AreEqual(0.2, result.Point.Y);
        }
    }
}
