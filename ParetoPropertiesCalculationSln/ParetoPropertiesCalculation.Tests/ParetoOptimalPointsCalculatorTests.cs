﻿using System.Collections.Generic;
using System.Linq;
using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using OxyPlot;
using Pareto;

namespace ParetoPropertiesCalculation.Tests
{
    [TestFixture]
    public class ParetoOptimalPointsCalculatorTests
    {
        [Test]
        public void Calculate_Test()
        {
            var paretoOptimalPointsCalculator = new ParetoDominatingPointsCalculator(new ParetoDominationCalculator());
            var points = new List<ParetoPoint>() {
                ParetoPoint.Factory.CreateParetoPoint("", new Point2D(0.1, 0.3), OxyColors.Black, MarkerType.Circle),
                ParetoPoint.Factory.CreateParetoPoint("", new Point2D(0.2, 0.2), OxyColors.Black, MarkerType.Circle),
                ParetoPoint.Factory.CreateParetoPoint("", new Point2D(0.6, 0.5), OxyColors.Black, MarkerType.Circle),
            };

            var (paretoDominatingPoints, paretoDominatedPoints) = paretoOptimalPointsCalculator.Calculate(points, OxyColors.Black, MarkerType.Circle);

            Assert.AreEqual(2, paretoDominatingPoints.Count());
            Assert.AreEqual(1, paretoDominatedPoints.Count());
            Assert.NotNull(paretoDominatingPoints.FirstOrDefault(x => x.Point == points[0].Point));
            Assert.NotNull(paretoDominatingPoints.FirstOrDefault(x => x.Point == points[1].Point));
            Assert.NotNull(paretoDominatedPoints.FirstOrDefault(x => x.Point == points[2].Point));
        }

        [Test]
        public void Calculate_EmptyInput_Test()
        {
            var paretoOptimalPointsCalculator = new ParetoDominatingPointsCalculator(new ParetoDominationCalculator());
            var points = new ParetoPoint[] { };

            var (paretoDominatingPoints, paretoDominatedPoints) = paretoOptimalPointsCalculator.Calculate(points, OxyColors.Black, MarkerType.Circle);

            Assert.AreEqual(0, paretoDominatingPoints.Count());
            Assert.AreEqual(0, paretoDominatedPoints.Count());
        }
    }
}
