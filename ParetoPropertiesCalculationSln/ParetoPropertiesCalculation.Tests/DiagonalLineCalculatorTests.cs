﻿using System;
using System.Collections.Generic;
using System.Linq;
using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using OxyPlot;
using Pareto;

namespace ParetoPropertiesCalculation.Tests
{
    [TestFixture]
    public class DiagonalLineCalculatorTests
    {
        [Test]
        public void Calculate_ThrowArgumentExceptionForLessThanTwoInputPoints_Test()
        {
            var points = new List<ParetoPoint>() { ParetoPoint.Factory.CreateParetoPoint("", Point2D.Origin, OxyColors.Blue, MarkerType.Circle) };
            var diagonalLineCalculator = new DiagonalLineCalculator();

            Assert.Throws<ArgumentException>(() => diagonalLineCalculator.Calculate(points, OxyColors.Black, LineStyle.Automatic));
        }

        [Test]
        public void Calculate_Test()
        {
            var points = new List<ParetoPoint>() {
                ParetoPoint.Factory.CreateParetoPoint("", new Point2D(0.1, 0.3), OxyColors.Black, MarkerType.Circle),
                ParetoPoint.Factory.CreateParetoPoint("", new Point2D(0.2, 0.2), OxyColors.Black, MarkerType.Circle),
                ParetoPoint.Factory.CreateParetoPoint("", new Point2D(0.6, 0.5), OxyColors.Black, MarkerType.Circle),
            };

            var diagonalLineCalculator = new DiagonalLineCalculator();

            var result = diagonalLineCalculator.Calculate(points, OxyColors.Black, LineStyle.Dash);
            var expectedResult = ParetoLine.Factory.CreateDiagonalLine("",
                new Point2D[] { new Point2D(0, 0), new Point2D(0.6, 0.6) }, OxyColors.Black, LineStyle.Automatic);

            Assert.AreEqual(expectedResult.Points.Length, result.Points.Length);
            Assert.AreEqual(expectedResult.Points.First(), result.Points.First());
            Assert.AreEqual(expectedResult.Points.Last(), result.Points.Last());
        }

        [Test]
        public void Calculate_SecondTest()
        {
            var points = new List<ParetoPoint>() {
                ParetoPoint.Factory.CreateParetoPoint("", new Point2D(0.2, 0.1), OxyColors.Black, MarkerType.Circle),
                ParetoPoint.Factory.CreateParetoPoint("", new Point2D(0.3, 0.2), OxyColors.Black, MarkerType.Circle),
                ParetoPoint.Factory.CreateParetoPoint("", new Point2D(0.3, 0.6), OxyColors.Black, MarkerType.Circle),
            };

            var diagonalLineCalculator = new DiagonalLineCalculator();

            var result = diagonalLineCalculator.Calculate(points, OxyColors.Black, LineStyle.Dash);
            var expectedResult = ParetoLine.Factory.CreateDiagonalLine("",
                new Point2D[] { new Point2D(0, 0), new Point2D(0.6, 0.6) }, OxyColors.Black, LineStyle.Automatic);

            Assert.AreEqual(expectedResult.Points.Length, result.Points.Length);
            Assert.AreEqual(expectedResult.Points.First(), result.Points.First());
            Assert.AreEqual(expectedResult.Points.Last(), result.Points.Last());
        }
    }
}
