﻿using System.Collections.Generic;
using System.Linq;
using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using OxyPlot;
using Pareto;
using ParetoPropertiesCalculation.Strategies;
using TcpNtcpCalculation;
using TcpNtcpCalculation.Helpers;
using TcpNtcpCalculation.Helpers.Immutables;
using TcpNtcpCalculation.Immutables;

namespace ParetoPropertiesCalculation.Tests
{
    [TestFixture]
    public class TcpNtcpCftcPointsToParetoPointsConverterTests
    {
        private TcpNtcpCftcPoint[] _points;

        [SetUp]
        public void SetUp()
        {
            _points = new TcpNtcpCftcPoint[]
            {
                new TcpNtcpCftcPoint(new TumorControlProbability(0.8), new NormalTissueComplicationProbability(0.2),
                    new ComplicationFreeTumorControl(0.64), new UTag("patient1", "C1", "P1")),
                new TcpNtcpCftcPoint(new TumorControlProbability(0.7), new NormalTissueComplicationProbability(0.15),
                    new ComplicationFreeTumorControl(0.595), new UTag("patient1", "C1", "P2")),
            };
        }

        [Test]
        public void Convert_OneMinusTcpAndNtcpStrategy_Test()
        {
            var strategy = new OneMinusTcpAndNtcpStrategy(new ComplicationFreeTumorControlCalculator(), 2);
            var tcpNtcpCftcPointsToParetoPointsConverter = new
                TcpNtcpCftcPointsToParetoPointsConverter(strategy);

            var expectedResult = new ParetoPoint[]
            {
                ParetoPoint.Factory.CreateParetoPoint(
                    ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                    {
                        ("Patient Piz", "patient1"), ("Course Id", "C1"), ("Plan Id", "P1"), ("TCP", 0.8.ToString()),
                        ("1-TCP", 0.2.ToString()), ("NTCP", 0.2.ToString()), ("P+", 0.64.ToString())
                    }), new Point2D(0.2, 0.2), OxyColors.Black, MarkerType.Circle),
                ParetoPoint.Factory.CreateParetoPoint(
                    ParetoTag.Factory.CreateParetoTag(new List<(string key, string value)>()
                    {
                        ("Patient Piz", "patient1"), ("Course Id", "C1"), ("Plan Id", "P1"), ("TCP", "0.7"),
                        ("1-TCP", 0.3.ToString()), ("NTCP", 0.15.ToString()), ("P+", 0.595.ToString())
                    }), new Point2D(0.3, 0.15), OxyColors.Black, MarkerType.Circle),
            };

            var result = tcpNtcpCftcPointsToParetoPointsConverter.Convert(_points);

            Assert.AreEqual(expectedResult.Length, result.Count());
            var tolerance = 0.001;
            foreach (var expectedParetoPoint in expectedResult)
            {
                Assert.NotNull(result.FirstOrDefault(x =>
                    x.Point.X > expectedParetoPoint.Point.X - tolerance &&
                    x.Point.X < expectedParetoPoint.Point.X + tolerance &&
                    x.Point.Y > expectedParetoPoint.Point.Y - tolerance &&
                    x.Point.Y < expectedParetoPoint.Point.Y + tolerance));
            }
        }
    }
}
