﻿using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using ParetoPropertiesCalculation.Strategies;
using TcpNtcpCalculation;
using TcpNtcpCalculation.Helpers;
using TcpNtcpCalculation.Helpers.Immutables;
using TcpNtcpCalculation.Immutables;

namespace ParetoPropertiesCalculation.Tests
{
    [TestFixture]
    public class ParetoBuildStrategiesTest
    {
        [Test]
        public void OneMinusTcpAndNtcpStrategy_Test()
        {
            var tcpNtcpCftcPoint = new TcpNtcpCftcPoint(new TumorControlProbability(0.925648365),
                new NormalTissueComplicationProbability(0.2537763),
                new ComplicationFreeTumorControl(0.6907407478292505), new UTag("Patient", "C", "Plan"));
            var point = new Point2D(1.0 - tcpNtcpCftcPoint.TumorControlProbability.Value,
                tcpNtcpCftcPoint.NormalTissueComplicationProbability.Value);
            var strategy = new OneMinusTcpAndNtcpStrategy(new ComplicationFreeTumorControlCalculator(), 2);

            var result1 = strategy.BuildTag(tcpNtcpCftcPoint);
            var result2 = strategy.BuildTag(point);

            Assert.AreEqual("Patient Piz:\tPatient\nCourse Id:\tC\nPlan Id:\tPlan\nTCP:\t0.93\n1-TCP:\t0.07\nNTCP:\t0.25\nP+:\t0.69", result1.ToString());
            Assert.AreEqual("TCP:\t0.93\n1-TCP:\t0.07\nNTCP:\t0.25\nP+:\t0.69", result2.ToString());
        }

        [Test]
        public void TcpAndOneMinusNtcpStrategy_Test()
        {
            var tcpNtcpCftcPoint = new TcpNtcpCftcPoint(new TumorControlProbability(0.925648365),
                new NormalTissueComplicationProbability(0.2537763),
                new ComplicationFreeTumorControl(0.6907407478292505), new UTag("Patient", "C", "Plan"));
            var point = new Point2D(tcpNtcpCftcPoint.TumorControlProbability.Value,
                1.0 - tcpNtcpCftcPoint.NormalTissueComplicationProbability.Value);
            var strategy = new TcpAndOneMinusNtcpStrategy(new ComplicationFreeTumorControlCalculator(), 2);

            var result1 = strategy.BuildTag(tcpNtcpCftcPoint);
            var result2 = strategy.BuildTag(point);

            Assert.AreEqual("Patient Piz:\tPatient\nCourse Id:\tC\nPlan Id:\tPlan\nTCP:\t0.93\nNTCP:\t0.25\n1-NTCP:\t0.75\nP+:\t0.69", result1.ToString());
            Assert.AreEqual("TCP:\t0.93\nNTCP:\t0.25\n1-NTCP:\t0.75\nP+:\t0.69", result2.ToString());
        }
    }
}
