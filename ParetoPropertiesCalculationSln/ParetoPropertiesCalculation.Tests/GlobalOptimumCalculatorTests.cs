﻿using System;
using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using OxyPlot;
using Pareto;
using ParetoFrontLineApproximation.Immutables;
using ParetoPropertiesCalculation.Strategies;
using TcpNtcpCalculation;

namespace ParetoPropertiesCalculation.Tests
{
    [TestFixture]
    public class GlobalOptimumCalculatorTests
    {
        [Test]
        public void Calculate_ThrowArgumentExceptionIfParetoFrontLineHasLessThanTwoVertices_Test()
        {
            var points = new Point2D[]
            {
                new Point2D(0.1, 0.7)
            };

            var paretoFrontLine = new ParetoFrontLine(points);
            var diagonalLine = ParetoLine.Factory.CreateDiagonalLine("",
                new Point2D[] { new Point2D(0.2, 0.1), new Point2D(0.9, 1) }, OxyColors.Black, LineStyle.Automatic);

            var globalOptimiumCalculator = new GlobalOptimiumCalculator(new OneMinusTcpAndNtcpStrategy(new ComplicationFreeTumorControlCalculator(), 2));
            Assert.Throws<ArgumentException>(() => globalOptimiumCalculator.Calculate(paretoFrontLine, diagonalLine, OxyColors.Black, MarkerType.Diamond));
        }

        [Test]
        public void Calculate_Test()
        {
            var points = new Point2D[]
            {
                new Point2D(0.1, 0.7),
                new Point2D(0.2, 0.3),
                new Point2D(0.5, 0.2),
                new Point2D(0.9, 0.1)
            };
            var paretoFrontLine = new ParetoFrontLine(points);
            var diagonalLine = ParetoLine.Factory.CreateDiagonalLine("",
                new Point2D[] { new Point2D(0.2, 0.1), new Point2D(0.9, 1) }, OxyColors.Black, LineStyle.Automatic);

            var globalOptimiumCalculator = new GlobalOptimiumCalculator(new OneMinusTcpAndNtcpStrategy(new ComplicationFreeTumorControlCalculator(), 2));
            var result = globalOptimiumCalculator.Calculate(paretoFrontLine, diagonalLine, OxyColors.Black, MarkerType.Triangle);
            var expectedResult = new Point2D(0.323, 0.265);

            Assert.NotNull(result);
            Assert.AreEqual(expectedResult.X, result.Point.X, 0.01);
            Assert.AreEqual(expectedResult.Y, result.Point.Y, 0.01);
        }

        [Test]
        public void Calculate_DuplicatePointsAsInput_Test()
        {
            var points = new Point2D[]
            {
                new Point2D(0.1, 0.7),
                new Point2D(0.2, 0.3),
                new Point2D(0.2, 0.3),
                new Point2D(0.5, 0.2),
                new Point2D(0.9, 0.1)
            };
            var paretoFrontLine = new ParetoFrontLine(points);
            var diagonalLine = ParetoLine.Factory.CreateDiagonalLine("",
                new Point2D[] { new Point2D(0.2, 0.1), new Point2D(0.9, 1) }, OxyColors.Black, LineStyle.Automatic);

            var globalOptimiumCalculator = new GlobalOptimiumCalculator(new OneMinusTcpAndNtcpStrategy(new ComplicationFreeTumorControlCalculator(), 2));
            var result = globalOptimiumCalculator.Calculate(paretoFrontLine, diagonalLine, OxyColors.Black, MarkerType.Triangle);
            var expectedResult = new Point2D(0.323, 0.265);

            Assert.NotNull(result);
            Assert.AreEqual(expectedResult.X, result.Point.X, 0.01);
            Assert.AreEqual(expectedResult.Y, result.Point.Y, 0.01);
        }
    }
}
