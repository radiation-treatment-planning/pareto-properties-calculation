﻿using MathNet.Spatial.Euclidean;
using NUnit.Framework;
using OxyPlot;
using Pareto;

namespace ParetoPropertiesCalculation.Tests
{
    [TestFixture]
    public class ParetoDominationCalculatorTests
    {
        [TestCase(0.3, 0.3, 0.4, 0.4, true)]
        public void IsDominating_Tests(double xPoint1, double yPoint1, double xPoint2, double yPoint2, bool expectedResult)
        {
            var point1 = ParetoPoint.Factory.CreateParetoPoint("", new Point2D(xPoint1, yPoint1), OxyColors.Black,
                MarkerType.Circle);
            var point2 = ParetoPoint.Factory.CreateParetoPoint("", new Point2D(xPoint2, yPoint2), OxyColors.Black,
                MarkerType.Circle);
            var paretoDominationCalculator = new ParetoDominationCalculator();
            Assert.AreEqual(expectedResult, paretoDominationCalculator.IsDominating(point1, point2));
        }
    }
}
