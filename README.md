A .Net Framework 4.7.2 library for calculating diverse Pareto properties: Pareto optimal points, illusion point, diagonal line through pareto front line, global optimum. For examples see the console application project. Here an example plot created with the console app:

![ExamplePlotOfParetoProperties](./ExamplePlotOfParetoProperties.png)